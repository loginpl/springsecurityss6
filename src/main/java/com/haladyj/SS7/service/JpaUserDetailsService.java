package com.haladyj.SS7.service;

import com.haladyj.SS7.model.User;
import com.haladyj.SS7.repository.UserRepository;
import com.haladyj.SS7.security.model.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var o = userRepository.findUserByUsername(username);
        User user = o.orElseThrow(()->new UsernameNotFoundException("Username not found"));
        return new SecurityUser(user);
    }
}


