package com.haladyj.SS7.security.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class UsernamePasswordCustomAuthentication extends UsernamePasswordAuthenticationToken {
    public UsernamePasswordCustomAuthentication(Object principal,
                                                Object credentials) {
        super(principal, credentials);
    }

    public UsernamePasswordCustomAuthentication(Object principal,
                                                Object credentials,
                                                Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
