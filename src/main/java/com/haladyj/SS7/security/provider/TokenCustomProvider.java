package com.haladyj.SS7.security.provider;

import com.haladyj.SS7.manager.TokenManager;
import com.haladyj.SS7.repository.UserRepository;
import com.haladyj.SS7.security.authentication.TokenCustomAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class TokenCustomProvider implements AuthenticationProvider {

    @Autowired
    private TokenManager tokenManager;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String token = authentication.getName();
        boolean exists = tokenManager.contains(token);

        if(exists){
            return new TokenCustomAuthentication(token,null,null);
        }
        throw new BadCredentialsException("Bad Credentials Token");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return TokenCustomAuthentication.class.equals(aClass);
    }
}
