package com.haladyj.SS7.security.provider;

import com.haladyj.SS7.repository.OtpRepository;
import com.haladyj.SS7.security.authentication.OtpCustomAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OtpCustomProvider implements AuthenticationProvider {

    @Autowired
    private OtpRepository otpRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String otp = String.valueOf(authentication.getCredentials());

        var o = otpRepository.findOtpByUsername(username);

        if(o.isPresent()){
            return new OtpCustomAuthentication(username,otp, List.of(()->"read"));
        }
        throw new BadCredentialsException("Bad Credentials");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return OtpCustomAuthentication.class.equals(aClass);
    }
}
