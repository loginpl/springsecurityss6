package com.haladyj.SS7.security.provider;

import com.haladyj.SS7.security.authentication.UsernamePasswordCustomAuthentication;
import com.haladyj.SS7.service.JpaUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UsernamePasswordCustomProvider implements AuthenticationProvider {

    @Autowired
    private JpaUserDetailsService jpaUserDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) {

        String username = authentication.getName();
        String password = String.valueOf(authentication.getCredentials());

        UserDetails ud = jpaUserDetailsService.loadUserByUsername(username);

        if(passwordEncoder.matches(password,ud.getPassword())){
            return new UsernamePasswordCustomAuthentication(username,password,ud.getAuthorities());
        }

        throw new BadCredentialsException("Bad Credentials");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UsernamePasswordCustomAuthentication.class.equals(aClass);
    }
}
