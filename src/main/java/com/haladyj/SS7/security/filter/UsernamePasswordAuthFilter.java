package com.haladyj.SS7.security.filter;

import com.haladyj.SS7.manager.TokenManager;
import com.haladyj.SS7.model.Otp;
import com.haladyj.SS7.repository.OtpRepository;
import com.haladyj.SS7.security.authentication.OtpCustomAuthentication;
import com.haladyj.SS7.security.authentication.UsernamePasswordCustomAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;

public class UsernamePasswordAuthFilter extends OncePerRequestFilter {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private OtpRepository otpRepository;

    @Autowired
    private TokenManager tokenManager;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filter) throws ServletException, IOException {

        var username = request.getHeader("username");
        var password = request.getHeader("password");
        var otp = request.getHeader("otp");

        if(otp == null){
            Authentication a = new UsernamePasswordCustomAuthentication(username,password);
            a = authenticationManager.authenticate(a);

            Otp otpInstance = new Otp();
            otpInstance.setOtp(generateOtp());
            otpInstance.setUsername(username);
            otpRepository.save(otpInstance);


        }else{
            Authentication a = new OtpCustomAuthentication(username,otp);
            a = authenticationManager.authenticate(a);

            String token = UUID.randomUUID().toString();

            tokenManager.addToken(token);

            response.setHeader("Token", token);
        }

    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return !request.getServletPath().equals("/login");
    }

    private String generateOtp(){
        return String.valueOf(new Random().nextInt(8999)+1000);
    }
}
