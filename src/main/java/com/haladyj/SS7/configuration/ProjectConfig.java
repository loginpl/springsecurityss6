package com.haladyj.SS7.configuration;

import com.haladyj.SS7.security.filter.TokenAuthFilter;
import com.haladyj.SS7.security.filter.UsernamePasswordAuthFilter;
import com.haladyj.SS7.security.provider.OtpCustomProvider;
import com.haladyj.SS7.security.provider.TokenCustomProvider;
import com.haladyj.SS7.security.provider.UsernamePasswordCustomProvider;
import com.haladyj.SS7.service.JpaUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
public class ProjectConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private TokenCustomProvider tokenCustomProvider;

    @Autowired
    private OtpCustomProvider otpCustomProvider;

    @Autowired
    private UsernamePasswordCustomProvider usernamePasswordCustomProvider;

/*    @Autowired
    private TokenAuthFilter tokenAuthFilter;

    @Autowired
    private UsernamePasswordAuthFilter usernamePasswordAuthenticationFilter;*/

    @Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean
    public JpaUserDetailsService userDetailsService(){
        return new JpaUserDetailsService();
    }

    @Bean
    public TokenAuthFilter tokenAuthFilter(){
        return new TokenAuthFilter();
    }

    @Bean
    public UsernamePasswordAuthFilter usernamePasswordAuthFilter(){
        return new UsernamePasswordAuthFilter();
    }

    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(otpCustomProvider);
        auth.authenticationProvider(usernamePasswordCustomProvider);
        auth.authenticationProvider(tokenCustomProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterAt(usernamePasswordAuthFilter(), BasicAuthenticationFilter.class)
            .addFilterAfter(tokenAuthFilter(),BasicAuthenticationFilter.class);
    }
}
